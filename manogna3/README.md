## ECE 445 Lab Notebook

## 8/25/23: First Meeting As A Group Talking About Ideas 
Objectives: Starting to discuss ideas for the project.       

Worked on: This meeting, we got together as a group and started discussing potential ideas for our project. One of the more serious project ideas we were considering was a Spice Dispenser project, which would consist of a mechanical functionality and allow a specific amount of a specific spice to be emitted by the spice dispenser using a digital UI. While we discussed this idea and talked to Jason Paximadas and other TAs, we came to the conclusion that it might not be as feasible - still considering it though. 

## 9/4/23: Ideation 
Objectives: Starting to listen to pitch projects and talk about more ideas for the project.      

Worked on: After thinking about the spice dispenser project, we put it on the web board; but it was already a project that was done in 2019. We considered modifying the project to contain voice recognition in order to allow a specfic spice to be dispensed; however, we deemed that this would become too difficult as we would have to either create our own model or use an existing model, which would not be as accurate. Due to this, we brainstormed another idea: a baby choking detector. This idea consisted of thinking about how to implement the choking detection. We believed that we could create a way to keep track of the breathing of a baby. We waited for feedbacki on this project idea. 

## 9/7/23: Meeting With Professor Gruev 
Objectives: Since the previous project idea is dismissed, we are considering doing a pitch project. 

Worked on: One of the pitch projects that sound interesting to us is the Surgical light pitched by Professor Gruev. During the meeting, we talk to him about the specifications. We need to use visible light, a non visible infrared light that can be used to illuminate cancerous cells. 

Some specifications include the white LEDs being around 1Watt in power and being very bright to the visible light. The NIR should be around 780nm and there should be a regular PCB and an LED PCB, specific for the LEDs that will be used in the project. Maybe use a metal core PCB so that the heat dissipation from the LEDs does not affect the PCB that will contain the microcontroller. The user of the device should be able to control the brightness somehow. 
We are mostly thinking of confirming this project. 

## 9/12/23: Meeting with TA 
Objectives: Meet with our TA Jason in order to connect with him on our goals for the semester. 

Worked on: We meet with 
Jason and show him our high level requirement for the project. We also talk about our initial block diagram that will be used for our design. Jason lets us know that the design looks ok, but the microcontroller needs to be picked so that the design and block diagram can be made more clear with the exact microcontroller that we will be working with. We decide to research different microcontrollers that we can use for our project. 

## 9/13/23: Work for Proposal Team Meeting 
Objectives: Work on the proposal and the contract so that our proposal can get approved. 

Worked on: Our team sat together and made sure to go through the different requirements for the proposal. We decide to use the ATMEGA328PB-AU as our microcontroller for the project. 
Used this datasheet to see the specifications: https://ww1.microchip.com/downloads/aemDocuments/documents/MCU08/ProductDocuments/DataSheets/40001906C.pdf
We also figure out the different subsystems that will be in our project. We will be having a subsystem for the microcontroller, the user interface, power supply, and LEDs. 

## 9/14/23: Finish up Proposal 
Objectives: Finish up the proposal to turn it in. 

Worked on: We talk about the tolerance analysis that we will need to use to analyze our design as well as worked on some ideas on how to develop and test our design.

## 9/21/23: Meeting With Professor Gruev 
Objectives: Meet with Professor Gruev to show him our design, requiements and block diagram. 

Worked on: We meet with the Professor and determine that we can potentially use a digital user interface if that seems more feasible. Furthermore, we talk about the idea to put as many LEDs as possible on the LED PCB(I believe he said 3x3 inches). He wanted the brightness of the LED to be as large as possible. 

## 9/24/23: Design Document Work 
Objectives: We decided to split up the work for the design doc

Worked on: We decided how to split the work. Jeremy and Yoga will be working on the LEDs and the power supply while I will be working mainly on the user interface and the microcontroller. We all decide to look into our respective parts. 

## 9/28/23: Design Document Completion
Objectives: Finish the design doc

Worked on: Since today is the deadline for the design doc, we all come together and finish it up with the research that we all did regarding our respective sections. We discussed each of our parts and determined that we would need a power supply for each subsection for our design doc. We also made sure to add LED drivers although we were not fully sure about how they would integrate with our design. I determined that the microcontroller would work for us since it had enough input and output pins. I also determined that we would use these specific UI components: 
https://www.digikey.com/en/products/detail/nkk-switches/A11JP/1052844
https://www.digikey.com/en/products/detail/bourns-inc/3310Y-002-104L/2534066

## 10/2/23: Work on Presentation 
Objectives: Prepare for the design doc review 

Worked on: We worked on the exact schematic for our design and made sure to decide which exact version of the microcontroller to use. We worked on determinig how we would make our PCB on Kicad. Tod, we started prepping for our presentation. 

## 10/4/23: Design Review Session 
Objectives: Had Design Review

Worked on: We made sure to finish up our schematic so we could present it in our design review. The feedback mostly came to help us fix our power supply, we will need to fix that later on. We also talked to Jason Paximadas and he says that everything can be simpler if we use 18650 batteries and use a boost and buck converters for our two different types of LEDs. 

## 10/6/23: PCB Progress 
Objectives: Work on PCB for first round of PCB review 
 
Worked on: We each work on our schematic sections and come together and I put the schematics together. We also start to work on the PCB part on Kicad. 


## 10/8/23: Rework on Proposal 
Objectives: See what points were missed on the proposal, and work on rewriting the proposal for the rewrite deadline. 

Worked on: Finished the rewrite using the feedback received from the Professor and the TAs. We fixed the power supply rework as we decided to use the 18650 batteries. Furthermore, we started creating the pcb on Kicad, and started wiring the PCB together. 

## 10/9/23: PCB First Round Due 
Objectives: Submit a PCB order 

Worked on: We ended up finishing the microcontroller PCB as we were able to wire all the components together and make sure that it fit the requirements for the PCB. We ran DRC, fixed the errors and then had a warning or error with some courtyard length situation, so we asked the head TA, who told us to submit the PCB for review anyway. The audit passed, so we submit our PCB orders. We also sent over the LED PCB since that PCB passed the audit on PCBway. 

## 10/18/23: First Round Ordering Parts 
Objectives: Order the list of parts we would need based on our first order of PCB. 

Worked on: We came up with a list of parts that we shared with each other on a spreadsheet which made it easier for us to order the list of parts and see which parts would be available in the ECE shop. 

## 10/20/23: Rewrite Design Doc 
Objectives: Finish rewriting the design doc in order to turn it in for a regrade. 

Worked on: We each made sure to look over our parts and based on the feedback, we were able to go through and modify our old design document. We made a more clear requirements and verifications table as specified to us during our design doc review, and we were able to modify subsystems like our power supply and how the subsystems were connected with each other. 


## 10/24/23: Meeting regarding work load for each of us 
Objectives: Discuss our workload and progress with Professor Fliflet 

Worked on: In this meeting today, we talked about how our LEDs being in parallel would cause a lot of problems. The head TA mentions that we should change how our LEDs are placed as well as talk about the goal of our project and a more even distribution of work moving forward. Each of us had to lean into our strengths more and communicate when each of us were able to and not able to meet. We also had to discuss with the head TA about our design changes; he let us know that it would be best if we were to use a boost and buck converter for our LED drivers which made us restart a big portion of our design. 

## 10/29/23: New Parts Necessary - New Order 
Objective: Complete our new order of parts for our modified PCBs. 

Worked on: We needed less parts with our new Boost and Buck converter, but we didn't have any of those parts from our previous order, so we had to put out a different order for the new parts needed. Our new LDO that would help with voltage supply for our microcontroller and UI was  https://www.digikey.com/en/products/detail/taiwan-semiconductor-corporation/TQL851CSV50-RLG/16610554. The output of this LDO would be 5V being supplied to both the microcontroller and the UI. There is a new schematic that was made today. 

![Screenshot_2023-12-07_at_8.39.59_PM](/uploads/3fe8a09873d3b3540e6ad3c2a017a9bb/Screenshot_2023-12-07_at_8.39.59_PM.png)

## 10/30/23: New PCB Kicad 
Objective: Change the PCB order 

Worked on: We had to finish redesigning the PCB layout since our design had completely changed from our first order. I mainly worked on the PCB with the microcontroller and making sure all the wiring for this was good. 

![Screenshot_2023-12-07_at_10.30.11_PM](/uploads/92be3e279a5bd00ec4a8bb3962e3ac3d/Screenshot_2023-12-07_at_10.30.11_PM.png)


## 11/1/23: Code and Order Purchase 
Objective: Make sure order for parts covered all the necessary components. 

Worked on: As we made sure that the components ordered were enough, we also made sure to figure out how to start writing code for our microcontroller in order for it to take in the five signals and then create a PWM output. We researched into this today. 

## 11/9/23: New PCB Arrived
Objective: Work on soldering the PCB

Worked on: Finally, today, we received our new PCB. From here, we started soldering parts onto our rechargeable PCB first in order to make sure we were finishing that as soon as possible and testing it with an LED. We didn't receive or order a stencil for this PCB, so we had to hand solder the parts. 

## 11/10/23: Reflow Oven 
Objective: Soldering parts onto microcontroller PCB 

Worked on: As we got to smaller parts on the microcontroller PCB, we realized we had to use our stencil in order to solder the smaller parts onto the main PCB. Most of the smaller solder mount components were put on using solder paste and the oven. 

## 11/13/23: Soldering the Microcontroller PCB 
Objective: Soldering the rest of the parts onto the microcontroller PCB

Worked on: After finishing up the oven soldering, we had to hand solder a lot of the commponents for the microcontroller PCB. Today, we also had to receive parts from the ECE shop in order to solder some resistors onto our PCB. 

## 11/14/23: Testing the microontroller
Objective: Start testing the soldered parts on the microcontroller. 

Worked on: Today, specifically, the blue light LED did not work and we had to go through and see what was causing that issue. We had to switch the blue light led that we were using since the infrared seller never got back to us and never supplied us with one. When we switched out our blue LED, we were able to get it to light up. Now we started to test our UI components by breadboarding using some non-solder mount potentiometers, leds, and switches. We had to make sure that we were sending the right input signals to the Arduino as well as setting the PWM output to a calculated value using the potentiometer voltage value. Originally, there was some flickering, but after some debugging, we were able to see the changes in input values show a change in the PWM output using the Arduino. 

## 11/17/23: Arduino Uno and Code 
Objective: Use Arduino code and integrate with the microcontroller. 

Worked on: Since we got the code working on the Arduino Uno, we had to now work on making sure that the code was aligning with the microcontroller and the pins that we were using on the microcontroller. This was not compiling today. 

## 11/27/23: LDO issues 
Objective: LDO figure out as well as code with the microcontroller. 

Worked on: We were facing issues with the Arduino code not being able to be programmed with the microcontroller. We think that it has to do with either a lack of compatibility in the bootloader or the clock speeds not being compatible. It might have also been the libraries that we got from github provided for the ATMEGA328PB. 

We also worked on the issue with the LDO not outputting 5V. It was outputting a really small voltage so we had to look into this issue. We realized that there was a short circuit for the LDO. We also decide to not use the microcontroller and instead just use the devboard for the demo. 

## 11/28/23: 2 High Level requirements working 
Objective: Test the PWM output and led lighting with the devboard since the LDO short circuited. 

Worked on: We ended up getting the blue/infrared LED to light up by testing and realizing that the infrared LED's driver was not connected to ground. Once we fixed this issue, we were able to get the blue light to light up. A delay for the PWM output was also added so there would be no flickering. 


## 11/29/23: Final Demo Day 
Objective: Demo our progress for our Professor and the TA

Worked on: We were able to finish most of our project as we spent all night trying to finish up the final demo and testing using the Arduino Uno. We demoed two of our highlevel requirements; unfortunately, we were not able to fully demo our last requirement as the boost converter was not working and our LDO short circuited.

![Screenshot_2023-12-07_at_10.11.28_PM](/uploads/1dfea4868239dca04df64c79cc3121b0/Screenshot_2023-12-07_at_10.11.28_PM.png)!

## 12/1/23: Mock Presentation 
Objective: Present during our session for the mock presentation practice run in order to receive feedback on our slides and etiquette. 

Worked on: We were able to present to a small audience as well as some evaluators, such as the grad student that gave us feedback on our presentation etiquette. We received good feedback and decide to take the small pointers in order to improve our slides as well. 

## 12/1/23: White LED Not working 
Objective: Try to get our white LEDs working so we could get our third high level requirement to be fulfilled. 

Worked on: We were able to g4et the White LEDs to work as well figure out how to fix our presentation based on the feedback that was received. 

## 12/5/23: Final Presentation
Objective: Do the final presentation and prep for it. 

Worked on: We made sure to be professional and be prepared for our final presentations by going through the flow and doing a practice round of our presentation. We not only did our final presentation, but we also made sure to redemo a part of our project for the Professor and TAs to see our white LEDs working with the correct voltage being supplied to the LED PCB. We also began to work on our final paper. 
![Screenshot_2023-12-07_at_10.11.37_PM](/uploads/665f3548a500561bf7ba3d71ff4a6bdb/Screenshot_2023-12-07_at_10.11.37_PM.png)!

## 12/6/23: Final Report 
Objective: Work on the Final Report for our project. 

Worked on: We worked on fully finishing our final report for our project as well as worked on the video for our extra credit. This was a very hectic day as we spent over six hours perfecting the final report. 

