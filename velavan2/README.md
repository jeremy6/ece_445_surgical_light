# 8.25.23: Initial Idea Brainstorming  
Objectives: Need ideas to propose  
What was done:  
                We were brainstorming ideas for our senior design project. I was thinking of some sort of idea to help with easier waking-up in the morning. It did not really seem feasible since it hard to implement and test over something so big like the bed. We then came up with issues that we were facing in our daily life and we decided on a maybe a spice dispenser. The TAs did have some suggestions. Possible motors. But still need a way to dispense only a certain amount of spice with certain weight. 

# 9.4.23: Second Brainstorming Idea   
Objectives: Need new ideas to propose  
What was done:  
                Unfortunately, our spice idea was already implemented a few years ago after we posted this on the Webboard. So we had to abandon this idea and instead come up with more ideas. We came up with a baby-choking detector and put this on the webboard. Awaiting feedback on this! 

# 9.7.23: Met with Professor Gruev
Objectives: Learn more about the pitched project
What was done: We had to expel the infant suffocation device since the TAs said this might be too hard to implement. 
        Learnt several requirements  
                -Need white LEDs: as bright as possible, 1 Watt  
                -Near infrared LED: 780nm: minimum 5 kilolux  
                -Adjustable Brightness
                -Possibly digital control            
                -Might require LED PCB (Metalcore I think)  
                -6x6 inch PCB  
                -No need optical filters? 
We also made the RFA this night as we needed to get the RFA approval.  

# 9.12.23: First TA Team Meeting
Objectives: Meet with our TA Jason  
What was done:  
                We brough our initial rendering of our block diagram with our TA Jason. We also brought our high-level requirements. We were told to decide on a microcontroller and improve the block diagram that I made.

![ECE445_BlockDiagramInitial-2](/uploads/e1296ff1128893e5049adebca1bdbda2/ECE445_BlockDiagramInitial-2.jpeg)   

# 9.13.23: Pre-Proposal Team Meeting
Objectives: Prepare for the Proposal  
What was done:  
                We started off by deciding on a microcontroller. This took quite some time to decide since none of our were very familiar with microcontrollers. We chose the ATMEGA328PB since it had a lot of pins and we would be going with a generous option, possible overkill. But we can always downsize later if we have to. I improved upon the block diagram after suggestions from our TA Jason with all the subsystems and we also came up with three high-level requirements. 
 
 ![image](/uploads/ffefafc751629d4baef7dcc1abdf5823/image.png)

# 9.14.23: Proposal and Team Contract  
Objectives: Work on the proposal and team contract 
What was done:  
                We worked on putting everything from the webboard idea into a proposal document. We refined it. We defined the subsystems, decided on the microcontroller fucntions, the tolerance analysis and also what requirements/tests we could perform. We also worked on the team contract. 

# 9.21.23: Meeting with Professor Gruev
Objectives: Refine details of the project  
What was done:  
                We met up with Professor Gruev, to determine the scope of the project and to refine the details of the project. Professor suggested using a digital user-interface if that would be more helpful. He also said to fit as many LEDs as possible on a 3x3 inch LED PCB. He also gave us the exact link to the LED that we should use for the infrared LED. Link is below:
                [link](https://www.lumixtar.com/smd3030-1w-ir-780nm-led.html)

# 9.24.23: Meet to work on the Design Doc
Objectives: Work on the design document and splitting up work  
What was done:  
                We met up and decided how we would split up the work on the schematics for the PCB. Jeremy would do power supply, I will do the LEDs and LED drivers and Manogna will do the user-interface and microcontroller. We also met up to work on the design document. Did not really get much done today as we were all busy. I worked on cleaning up the block diagram more as there were parts of the block diagram especially the power lines that needed to be clarified.
                ![Screen_Shot_2023-12-07_at_12.07.03_PM](/uploads/24daa4a053e3cb3692c4c291d3c657a9/Screen_Shot_2023-12-07_at_12.07.03_PM.png)

# 9.28.23: Design Document
Objectives: Work on the design document 
What was done:  
                Today was the deadline to submit the design document. We individually had our parts done and now haad to integrate it together. We used multiple LDOs for the batteries. We refined more about the requirements and verifications. We were unsure of exactly how feasible the multiple batteries would be. Also we were not sure how the LED drivers would exactly work but our TA Jason provided us with a link to an LED driver that would buck the LED voltage so we used this. 


[link](https://www.ti.com/product/TPS922052)
# 10.2.23: Work on presentation and pcb
Objectives: Need to prepare for the design document review
What was done:  
                Today we had to start working on the PCB after meeting with our TA Jason. He said that at most only one of us could do the PCB layout. We were still making our schematic at this point. We also discusses potential questions for our design document review.

# 10.3.23: Work more on the PCB design
Objectives: Still making more schematics decisions
What was done:  
                We were still consulting the datasheets to finish up the schematics. Not much was done today either as we had to leave early as we were all so busy.   

# 10.4.23: Design Review
Objectives: Design Review Presentation
What was done:  
                We had our design review today. There were several things that we could improve upon. Mainly the supply source. We were brainstorming ideas later to improve upon this supply source issue. Everything else seemed good. We talked to head TA Jason about it and we told us to just use buck and boost converters and use one power supply. He had us look into 18650 batteries. 

# 10.5.23: Design Review
Objectives: Change our design yet again
What was done:  
                We started by changing our power supply. This meant we had to either resize or entirely change the other components from other parts of the design especially the LED drivers which was what I was working on. We added several buck and boost converters to deal with this issue. While some of us where still researching the components, we also starting working on the schematic for the UI since that was not being changed yet. Here are the links to the buck and boost converters I decided upon:
                [link](https://www.ti.com/product/TPS560200/part-details/TPS560200DBVR) 
                [link](https://www.digikey.com/en/products/detail/texas-instruments/TPS7B8601QDDARQ1/13627091)

# 10.6.23: PCB Review
Objectives: Had to get an opinion of our current PCB layout
What was done:  
                While we were not entirely done with our PCB layout, we were done with the schematic part. So we brought in our schematic to the TAs for PCB review to hear their opinion on what we were doing. They said the microcontroller and UI looked fine. We ran out of time to check about the rest of the schematics. 

# 10.8.23: Revised Proposal Document
Objectives: Revise the original proposal 
What was done:  
                Since we lost a few points on the proposal, we met up again to submit a revised proposal document. We updated the schematics. We also followed several of our TA, Jason's comments like the fact that there were not any voltage regulators on our original proposal. Also he noted that we needed to be more quantitative with our requirements. 

# 10.9.23: First PCB Due
Objectives: Submit our first PCB order 
What was done:  
                Our first PCB order is due tomorrow. So we had to quickly work to finish our PCB layout. We had some issues with running the DRC. Some of our footprints' courtyards were beyod the edges. Although we did not know how to fix this, when we ran through the audit, it went through fine. We will have to discuss more with our TA, Jason tomorrow during our meeting. 

# 10.18.23: Order First Round of Parts
Objectives: Order parts 
What was done:  
                Since we were so busy this week, we were not able to meet earlier. But we did end up making a list of items we would need. Today we met up to start placing the order. We were unsure of how to proceed with ordering but we got some help from our TA Jason who showed us the document to place orders through the ECE purchasing department. 

# 10.20.23: Design Document Rewrite
Objectives: Rewrite the design document
What was done:  
                We lost a few points on the design document. So we had to meet up to correct these errors. We also took note from the design document review. We firstly made changes to the battery supply. Then we put the requirements and verifications into a table. We did not do much apart from this and we were just awaiting our parts to arrive. 

# 10.24.23: Meeting with Proffesor Fliflet
Objectives: Discuss our team issue with the Proffesor Fliflet
What was done:  
                We had a meeting today with Professor Fliflet about the dynamics of team working. He noticed an uneven workload distribution so we had a guidelines about that. We also entirely had to change our design as using multiple buck converters and boost converters along with the LED drivers reduced the efficiency of the system. Moreover, the way that we had placed our LEDs in parallel would not really be feasbile. So i had to look more into combinging the LED drivers and the voltage converters for that. 

# 10.29.23: Ordering New Parts
Objectives: Finish ordering the new parts
What was done:  
                Since our meeting, our design had been changed for the most part. So it meant ordering new parts and redoing the schematics and the PCB layout. After much research on LED drivers, I finally decided upon the following two LED drivers for the white and the infrared LED respectively. I chose the white light LED driver since it had dual output and it performs boosting action. I chose the infrared light LED driver since it was simple and had a good bucking action. We are in the process of ordering the parts since we had already made the schematics.  
                [link](https://www.digikey.com/en/products/detail/renesas-electronics-corporation/ISL97682IRTZ-TK/2635839)
                [link](https://www.digikey.com/en/products/detail/texas-instruments/TPS54200DDCR/8133009)
    
# 10.30.23: PCB Layout
Objectives: Need to make PCB Laout for PCB Ordering
What was done:  
                We mostly worked alone this week as we had different PCBs to do the layour for. I did the PCB layour for the LEDs which Manogna and Jeremy did the PCB layout for the the main PCB. I initally thought we could do a metal-core PCB for the LEDs for better heat dissipation but since this was too expensive, I tried doing it on a single layer to bring down the costs.

# 11.1.23: Team Meeting
Objectives: Go over our purchase order and work on code
What was done:  
                We checked out an Arduino dev board so at this point, we were testing out the code out it. We made a makeshaft voltage divider circuit and then used this to see if the Arduino was reading the correct voltage and then we used the output pins to produce PWM to a test LED. We had a few bugs but we fixed them.

# 11.9.23: Soldering PCB
Objectives: Soldering Rechargable Battery PCB
What was done:  
                We soldered all the components onto the rechargable battery PCB. We had to hand-solder them as we did not have a stencil for this. However, we did have a wall adapter to barrel jack converter so we could not test this circuit yet.

# 11.10.23: Using the reflow oven 
Objectives: Over soldering the main PCB
What was done:  
                We used the reflow oven to solder the main PCB. This was a lengthy process as most of the components were very small and had a hard time placing them on the board. We had a few issues with the size of the 10kOhm resistor ordered being too small for the footprint size on the PCB. So we did not start working on the main PCB yet. Then we worked further on the code on the Arduino so we can just put it onto the microcontroller later.

# 11.13.23: Soldering the rest of the main PCB 
Objectives: Finish hand soldering the main PCB
What was done:  
                We were able to acquire the 10kOhms from the ECE service shop with the help of our TA Jason Zhang so we used this to hand solder it onto the main PCB. We will start testing from tomorrow. 

# 11.14.23 - 11.16.23: Testing the microcontroller 
Objectives: Get the microcontroller to work 
What was done:  
                We were able to get the code on the Ardunio Uno but when we tried to program the code onto the microcontroller on our PCB. We kept getting an error. This was quite a challenge to debug as everything seems fine including the connections and pin solderings. We tried multiple times with several programmers and several libraries online. However, nothing seemed to work so we had to unfortunately and reluctantly give up on the microcontroller and start testing on the development board.


# 11.17.23: Integrating our PCB with the development board 
Objectives: Make the PCB work in tandem with the dev-board
What was done:  
                Since our microcontroller on the PCB did not work, we starting soldering some wires onto our PCB that would connect to the dev board directly. This was quite tedious as we had to be very careful when soldering additional components onto our PCB. Moreover, we had to ensure that the microcontroller on the dev-board was compatible with the code that we had modified again for the microcontroller on our PCB. Our LDO seemed to be working fine. So we were able to test the user-interface and the LDO with the user-interface on the Arduino. It was able to read the values well. We will start testing the rest of the components after break.

# 11.27.23: Fixing the LDO 
Objectives: Fix the LDO
What was done:  
                Our LDO shortcitcuited during testing and eventually did not produce any voltage drop across it. This meant we had to move the user-interface from the PCB to a bread board, so that it could continue working without malfucntioning. We presume that this short-circuited happened because we were passing too much power for the LDO to handle. 
                
# 11.28.23: Fixing the infrared LED 
Objectives: Fix the infrared LED driver
What was done:  
                Initially the infrared LED driver was not working. We kept trying to debugging it to see if there was a short-circuit on the legs of the blue LED driver. Since there was not a short circuit on the legs of the infrared LED driver, we were testing to see if the ground pins were connected. After much time spent on this, we realized the infrared LED driver's ground was not connected to the rest of the PCB's ground. So we went ahead and connected this using a wire.

![Screen_Shot_2023-12-07_at_3.05.53_PM](/uploads/9eb2052a3ec989632749a934086a678a/Screen_Shot_2023-12-07_at_3.05.53_PM.png)
# 11.29.23: Fixing the white LEDs 
Objectives: Fix the white LED driver
What was done:  
                We were not able to figure out why the white LED driver would not work. So we spent the whole day debugging the working of the white LED driver. We tried unsoldering the component and trying to see if there was any stray flux that could have caused the malfunctioning of the component. We realized that the ground pins were not connected so we tried connecting that to see if that would fix the issue. It did not really help so we had to conclude that the white LED driver had a component failure.

# 12.1.23: Final Demo 
Objectives: Present during Final Demo
What was done: 
                We were able to demonstrate the working of two out of our three high-level requirements (infrared LED and the user-interface). However we were not able to get the white LEDs to work. However, after the demo we were able to get it to turn on using an external power supply.
                ![Screen_Shot_2023-12-07_at_3.05.33_PM](/uploads/a2f53dff360b9b427575aa8e80ac4d6c/Screen_Shot_2023-12-07_at_3.05.33_PM.png)

# 12.3.23: Mock Presentation 
Objectives: Present during Mock Presentation
What was done:  
                We presented during Mock Demo anf got some great feedback from the audience and evaluators on how to improve our slides and our presentation style. 

# 12.5.23: Final Presentation 
Objectives: Final Presentation
What was done:  
                We presented during our final presentation about our project. We also redemoed an additional working part of our project. The white light LEDs were now able to light up. Then we worked on the final paper together.

# 12.6.23: Final Paper
Objectives: Finish final paper
What was done:  
                We met up to finish the final paper. We were able to finish it and submit it on time to the myPACE section of the website.
