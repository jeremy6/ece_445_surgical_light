# ECE_445_Surgical_Light



## Team Members:

Manogna Rajanala - manogna3@illinois.edu
Jeremy Wu - jeremy6@illinois.edu
Yogavarshini Velavan- velavan2@illinois.edu

## PROBLEM:
Surgeons and medical professionals removing cancerous cells, mostly use their sense of vision to determine which cells are malignant and appropriately remove those. However, there is a limit to the human vision especially when dealing with an entity like the human body which is so complex and small like cancer cells. Considering how life threatening cancerous growths are and the fact that cancer is the second most leading cause of death in humans, detection and removal of cancerous cells is of utmost importance. Therefore, there is a critical and growing need to develop tools and methods to aid surgeons in their job of identifying and eliminating cancer cells.

## SOLUTION:
Our solution to this is two-pronged: a microscopic camera and a surgical light. Our team will be working on the surgical light. This lamp will work in tandem with the microscopic camera to better aid cancer specialists to identify cancerous growths during both surgery and early examination. The surgical light solution is a programmable light source that will mainly be used in surgical settings.The surgical light will have different LEDs that will allow the user to modify the brightness of the light as they deem appropriate. The microcontroller will allow for the adjusting of the brightness and this could be with or without a wire. Additionally, an additional LED PCB will be used in order to allow for heat dissipation and terminal release.

The light sources will contain different sets of LEDs. The first set of LEDs would be visible spectrum white LEDs (~400-700nm). The second set of LEDs would emit around 700-800nm infrared light.

## SOLUTION COMPONENTS:
- Two layer heat dissipating PCB for LED that is different from a regular PCB because otherwise the PCB will melt
- Infrared light that will be around 700-800nm, minimum 1 milliwatt per cm square
- White LEDs that are around 400-700nm, minimum of 5 kilolux
- LED drivers

## CRITERION FOR SUCCESS:
- Detecting cancerous cells when the surgical light along with the microscopic camera is shown
- User able to increase/decrease the brightness of the light and the color temperature from a pc
- UI for the user interaction with the LEDs