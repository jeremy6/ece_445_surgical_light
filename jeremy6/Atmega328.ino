// #define F_CPU 8000000UL
// #include <avr/io.h>
// #include <util/delay.h>

// void ADC_init() {
//     // Set voltage reference to AVcc, select ADC channel and enable ADC
//     ADMUX = (1 << REFS0) ;
//     ADCSRA = (1 << ADEN)| (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
// }

// void PWM_init()
// {
// 	/*set fast PWM mode with non-inverted output*/
// 	// TCCR0A = (1<<WGM01) | (1<<WGM01) | (1<<COM0A1) | 1<< COM0B1 | (1<<CS00);
//   // TCCR0B = (1<< CS02);
//   TCCR0A |= _BV(COM0A1) | _BV(WGM00) | _BV(WGM01);
//   TCCR0B |= _BV(CS00);
// }

// void setPWMDutyCycle(uint8_t dutyA, uint8_t dutyB) {
//     // Set PWM duty cycle for PD1 (OC0A) and PD0 (OC0B)
//     OCR0A = dutyA;
//     OCR0B = dutyB;
// }

// uint16_t readADC(uint8_t channel) {
//     // Select ADC channel and start conversion
//     ADMUX = (ADMUX & 0xF0) | (channel & 0x0F);
//     //ADCSRA |= (1 << ADSC);

//     // Wait for conversion to complete
//     //while (ADCSRA & (1 << ADSC));

//     // Return ADC result
//     return ADC;
// }
// void setup()
// {
//     // DDRB |= (1 << DDB1);
//     DDRD |= _BV(PD6);
//     DDRB &= ~(1 << DDB0);
//     DDRD &= ~(1 << DDD7);
//     DDRC &= ~(1 << DDC0);
//     //DDRE &= ~(1 << DDE7);
//     DDRB &= ~((1 << DDB0) | (1 << DDB1));
//     PORTB |= (1 << PORTB0) | (1 << PORTB1);
// }

// int main ()
// {
// 	unsigned char duty;
//   setup();
// 	ADC_init();
//   PWM_init();
//   bool temp = false;
// 	while (1)
// 	{
//     // uint16_t A1 = readADC(1);
//     // uint16_t A2 = readADC(2);
//     // uint16_t A3 = readADC(3);
//     // uint16_t A4 = readADC(4);
//     // uint16_t A5 = readADC(5);

//     uint32_t dutyA = 0;
//     uint32_t dutyB = 0;
//     // if (PINB & (1 << PINB0))
//     // {
//     //   if (PIND & (1 << PIND7))
//     //   {
//     //     //duty = ceil(/4);
//     //     OCR0A = 128;
//     //   }
//     //   // if ( == 1)
//     //   // {
//     //   //   uint32_t duty = ceil((A(smth))/4);
//     //   //   OCR0A = duty;
//     //   // }

//     // }
//     // if(temp){
//     //   dutyA = 128;
//     //   temp = false;
//     // }
//     // else
//     // {
//     //   temp = true;
//     //   dutyA= 0;
//     // }
//     for(int i=0; i<255; i++)
//     {
//       // setPWMDutyCycle(dutyB,i);
//       OCR0B = i;
// 		  _delay_ms(10);
//     }
//     for(int i=255; i>0; i--)
//     {
//       // setPWMDutyCycle(dutyB,i);
//       OCR0B = i;
// 		  _delay_ms(10);
//     }
// 	}
// }








// #include <avr/interrupt.h>
// #include <stdbool.h>
// #include <util/delay.h>
// int main (void) {

//     /**
//     * We will be using OCR0A as our PWM output register which is the
//     * same pin as PD6.
//     */
//   DDRD |= _BV(PD6);
//     /**
//     * There are quite a number of PWM modes available but for the
//     * sake of simplicity we'll just use the 8-bit Fast PWM mode.
//     * This is done by setting the WGM00 and WGM01 bits.  The
//     * Setting COM0A1 tells the microcontroller to set the
//     * output of the OCR0A pin low when the timer's counter reaches
//     * a compare value (which will be explained below).  CS00 being
//     * set simply turns the timer on without a prescaler (so at full
//     * speed).  The timer is used to determine when the PWM pin should be
//     * on and when it should be off.
//     */
//   TCCR0A |= _BV(COM0A1) | _BV(WGM00) | _BV(WGM01);
//   TCCR0B |= _BV(CS00);
//     /**
//     *  This loop is used to change the value in the OCR0A register.
//     *  What that means is we're telling the timer waveform generator
//     *  the point when it should change the state of the PWM pin.
//     *  The way we configured it (with _BV(COM0A1) above) tells the
//     *  generator to have the pin be on when the timer is at zero and then
//     *  to turn it off once it reaches the value in the OCR0A register.
//     *
//     *  Given that we are using an 8-bit mode the timer will reset to zero
//     *  after it reaches 0xff, so we have 255 ticks of the timer until it
//     *  resets.  The value stored in OCR0A is the point within those 255
//     *  ticks of the timer when the output pin should be turned off
//     *  (remember, it starts on).
//     *
//     *  Effectively this means that the ratio of pwm / 255 is the percentage
//     *  of time that the pin will be high.  Given this it isn't too hard
//     *  to see what when the pwm value is at 0x00 the LED will be off
//     *  and when it is 0xff the LED will be at its brightest.
//     */
//   while(1)
//   {
//     uint8_t pwm = 0x00;
//     bool up = true;
//     Serial.begin(9600);
//     Serial.print("hello");
//     for(int p=0; p< 255; p ++)
//     {
//       OCR0A = pwm;
//       //pwm += up ? 1 : -1;
//       pwm ++;
//       // if (pwm == 0xff)
//       //   up = false;
//       // else if (pwm == 0x00)
//       //   up = true;
//       _delay_ms(1000);
//       //Serial.print("PWM: ");
//       Serial.println(p);
//       // Print the variable value to the Serial Monitor
//     }

//     for(int p=255; p>0 ; p --)
//     {
//       OCR0A = pwm;
//       pwm --;
//       _delay_ms(10);
//     }



//   // Add a delay to make the output more readable
//   //delay(1000);
//   }
// }
#include <avr/io.h>
#include <util/delay.h>

int previousStateMaster = 1;
int previousStateWhite = 1;
int previousStateRed = 1;
int previousWhite = 0;
int previousRed = 0;
void setup() {
  // Initialize Serial communication with a baud rate of 9600
  pinMode(PD4, INPUT);
  pinMode(PD2, INPUT);
  pinMode(PD7, INPUT);
  //pinMode(PD6, OUTPUT);
  Serial.begin(9600);

  // Wait for the Serial Monitor to open
  while (!Serial) {
    // Wait for the Serial Monitor to open
  }

  // Print a message when the program starts
  Serial.println("Arduino Uno is ready!");
}

// int main()
// {
//   setup();
//   DDRD |= (1<<PD5);
//   // analogReference(INTERNAL);
//   // OCR0A = 255;
//   // TCCR0A |= (1 << WGM01) | (1 << WGM00); // Fast PWM mode
//   // TCCR0A |= (1 << COM0A1); // Set OC0A on compare match
//   // TCCR0B |= (1 << CS00); // Set prescaler (e.g., 8)
//   // TCCR0A |= (1 << WGM01) | (1 << WGM00); // Fast PWM mode
//   // TCCR0A |= (1 << COM0B1) | (1 << COM0B0); // Set OC0B on compare match
//   // TCCR0B |= (1 << CS01); // Set prescaler (e.g., 8)

//     // Set an initial value for the duty cycle (0 to 255)
//   // OCR0B = 128;
//   DDRD |= (1<<PD6);
//   DDRD |= (1<<PD5);
//   //Fast PWM output at OCOA pin
//   OCR0A = 0; // Duty cycle of 75%
//   OCR0B = 0;
//   TCCR0A |= (1<<COM0A1) |(1<<COM0B1) |  (1<<WGM01) | (1<<WGM00) ;
//   TCCR0B |= (1<<CS00) ;
//   // PORTB |= (1 << PORTB0) | (1 << PORTB1);
//   while (1)
//   {
//     // if (PINB & (1 << PINB0))
//     // {
//     //   if (PIND & (1 << PIND7))
//     //   {
//     int sensorValue = analogRead(A0);
//     Serial.println(sensorValue);
//     // for(int k=0; k<255; k++)
//     // {
//     //   OCR0A = k;
//     //   OCR0B = 255-k;
//     //   _delay_ms(10);
//     // }
//     // for(int k=255; k>0; k--)
//     // {
//     //   OCR0A = k;
//     //   OCR0B = 255-k;
//     //   _delay_ms(10);
//     // }
//   }

//   // while (1)
//   // {
//   //   PORTD |= (1<<PD5);
//   //     // for(int k =0; k<200; k++)
//   //     // {
//   //     //   OCR0B = k;
//   //     //   //OCR0A = k + 2;
//   //   Serial.println("here");
//   //   _delay_ms(500);
//   //   // }
//   //   PORTD &= ~(1<<PD5);
//   //   _delay_ms(500);
// 	// }
//    return 0;
//  }
// int main()
// {
//   Serial.print("hi");
//   DDRD |= (1<<PD6); // change this to 0
//   DDRD |= (1<<PD5); // change this to 1
//   //Fast PWM output at OCOA pin
//   OCR0A = 0; // Duty cycle of 75%
//   OCR0B = 0;
//   TCCR0A |= (1<<COM0A1) |(1<<COM0B1) |  (1<<WGM01) | (1<<WGM00) ;
//   TCCR0B |= (1<<CS00);
//   return 0;
// }
// the loop routine runs over and over again forever:
// int main()
// {
//   TCCR3A |= (1 << COM3A1) | (1 << COM3B1) | (1 << WGM31) | (1 << WGM30);
// }
void loop() {
  // read the input on analog pin 0:
  Serial.print("help");
  int potentWhiteLed = analogRead(A0);
  int potentInfraLed = analogRead(A5);   // 7 doesn't exist on this ardunio
  int infraLedSwitch = digitalRead(PD4);  // 6 doesnt exit on this ardunio
  int masterSwitch = digitalRead(PD2);
  int whiteLedSwitch = digitalRead(PD7);
  DDRD |= (1 << PD6);  // change this to 0
  DDRD |= (1 << PD5);  // change this to 1
  // //Fast PWM output at OCOA pin
  OCR0A = 0; // Duty cycle of 75%
  OCR0B = 0;
  TCCR0A |= (1 << COM0A1) | (1 << COM0B1) | (1 << WGM01) | (1 << WGM00);
  TCCR0B |= (1 << CS00);
  // TCCR3A |= (1 << COM3A1) | (1 << COM3B1) |  (1 << WGM30);
  // TCCR3B |= (1 << CS30) | (1 << WGM32);
  // TCCR4A |= (1 << COM4A1) | (1 << COM4B1)  | (1 << WGM40);
  // TCCR4B |= (1 << CS40)| (1 << WGM42);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  //float voltage = infraLedSwitch * (5.0 / 1023.0);
  // for(int k=0; k<255; k++)
  // {
  //   OCR0A = k;
  //   OCR0B = 255-k;
  //   _delay_ms(10);
  // }
  // for(int k=255; k>0; k--)
  // {
  //   OCR0A = k;
  //   OCR0B = 255-k;
  //   _delay_ms(10);
  // }
  if(previousStateMaster != masterSwitch || previousStateWhite != whiteLedSwitch || previousStateRed !=  infraLedSwitch)
  {
    _delay_ms(100);
    infraLedSwitch = digitalRead(PD4);  // 6 doesnt exit on this ardunio
    masterSwitch = digitalRead(PD2);
    whiteLedSwitch = digitalRead(PD7);
    if (masterSwitch == 1) {
      if (whiteLedSwitch == 1) {
        //int temp = potentWhiteLed * (255) / 1023;
        int temp = map(potentWhiteLed, 0, 1023, 0, 255);
        int duty = (temp * 100) / 255;
        OCR0A = duty;  // this register is OC4A
        //OCR0A = temp;  // this register is OC3A
        //OCR3A = temp;
        // Serial.print("me ");
        // Serial.println(temp);
      }
      else
      {
        OCR0A = 0;
      }
      if (infraLedSwitch == 1) {
        int temp = map(potentInfraLed, 0, 1023, 0, 255);
        int duty = (temp * 100) / 255;
        OCR0B = duty;  // this register is OC4A
        //OCR4A = temp;
        // Serial.print("try ");
        // Serial.println(temp);
      }
      else
      {
        OCR0B = 0;
      }
    }
    else
    {
      OCR0A = 0; // Duty cycle of 75%
      OCR0B = 0;
    }
  }
  else if(previousWhite != potentWhiteLed || previousRed != potentInfraLed)
  {
    if (masterSwitch == 1)
    {
      if (whiteLedSwitch == 1) {
        // int temp = potentWhiteLed * (255) / 1023;
        int temp = map(potentWhiteLed, 0, 1023, 0, 255);
        int duty = (temp * 100) / 255;
        OCR0A = duty;  // this register is OC4A
        Serial.print(" ");
        Serial.print(temp);
        Serial.print(" ");
        Serial.println(duty);
      }
      if(infraLedSwitch == 1)
      {
        int temp = map(potentInfraLed, 0, 1023, 0, 255);
        int duty = (temp * 100) / 255;
        OCR0B = duty;  // this register is OC4A
        //int temp2 = potentInfraLed * (255) / 1023;
        //OCR0B = temp2;  // this register is OC4A
      }
    }
  }
  // print out the value you read:
  Serial.print(" ");
  Serial.print(masterSwitch);
  
  Serial.print(" ");
  Serial.print(whiteLedSwitch);
  Serial.print(" ");
  Serial.print(infraLedSwitch);
  Serial.print(" ");
  Serial.print(potentWhiteLed);
  Serial.print(" ");
  Serial.println(potentInfraLed);
  previousStateMaster = masterSwitch;
  previousStateWhite = whiteLedSwitch;
  previousStateRed = infraLedSwitch;
  previousWhite = potentWhiteLed;
  previousRed = potentInfraLed;
  _delay_ms(1000);
}
