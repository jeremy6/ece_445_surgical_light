## ECE 445 Lab Notebook

### 8/25/23 First Meeting for Ideas
Objective: come up with an idea to build into an project
There were a few ideas tossed around, the one we thought we could use was the spice dispensor where a dispensor would dispense based on the amount of spice inputted from a digital UI and the type of spice.  

### 9/4/23 Second Meeting about Ideas
Objective: come up with another idea that can be a project
The previous project was rejected and head TA jason said its not that great either. Voice recognition projects aren't great, AI project aren't great either. We now are proposing the baby tool that can help parents know when an infant is sufficating

### 9/7/23 Meeting with Professor
Objective: Since the previous project have been rejected, we are going to do a pitched project. We are meeting the Professor Gruev to talk about his project.
Notes from the meeting: 
Objective: design a programmable light source can be used in a surgical setting, help surgeon use this light to detect cancer cells, use LED 

Have to have a visual spectrum led, and have another set of LED 180 nanometer in wavelength, near infrared, 400-700 nanometer, don’t want to go into UV spectrum, don't go beyond that spectrum, the white LED, as bright as possible, 20,000 kilo, minimum 5 kilo, adjustable brightness, digital control, 700-800 neored nanometer LED, no more than min 1 milwatt per square, the brightness is adjustable by operator, size = 6x6 inches PCB board, have to choose LED, neored, optical filters prefer not to, will have LED drivers, bright LED need led driver, amplifier, LED drivers are chips, LED 1 watt -> 1 watt LED Driver, fabricating this on a specialized pcb, two layers, made to dissipate heat, thermal paste, two layer pcb for LED,  microcontroller controls the brightness, wire wireless no matter, communicate to a pc, add a graphical settings, color temperature as a setting, 

Need: Regular pcb and LED pcb

Additionally, we met again afterwards to complete our RFA and used the notes we took from the meeting to write the RFA.

### 9/11/23 Work on Proposal
Objective: Split up the work for the Proposal
We went through the proposal on the ECE 445 website and split up what each person should do for the project.

### 9/12/23 First TA meeting
Objective: Meet with the TA and talk about the project that got approved
We went over the project like the block diagram, high level requirements. However, we were asked to decided a microcontroller since we haven't chosen one yet.

### 9/13/23 Team meeting before Proposal
Objective: Work the proposal and the contract
We decided to use ATmega328PB-AU for our microcontroller. We also drew a rough block diagram on what we think our project is going to be (see yoga's folder to view the block diagram). In that drawing, we identified the subsystems of the project which were the leds, the user interface, the microcontroller and the power supply. 

We also start the contract for our group.

Used code of ethics website: https://www.ieee.org/about/corporate/governance/p7-8.html

### 9/14/23 Team meeting day the Proposal was due
Objective: Finish proposal and contract
We finished the Proposal before we zoomed to do the tolerance analysis. Additionaly, we finished the contract to sign before turning it in.

reference used to understand how the infrared led works: https://www.pennmedicine.org/cancer/navigating-cancer-care/treatment-types/surgery/tumorglow#:~:text=Before%20surgery%2C%20patients%20are%20injected,tissue%20in%20areas%20previously%20undetected

### 9/21/23 Second meeting with Profressor Gruev
Objective: Talk with Professor Gruev to confirm details about the project for the design doc
We met Professor Gruev to determine the scope of the project and see if he has any feeback for our block diagram. Professor Gruev suggested that we use a digital UI. He also asked to have the board have as LEDs as possible since he wants it to be as bright as possible. 

Link to infrared: https://www.lumixtar.com/780nm-1w-3w-smd3535-infrared-high-power-led.html


### 9/24/23 LED decisions
Objective: We split up the Design Document
I was tasked to determine how the LEDs whould be laid out as the they would be determined by the power supply. I decided to split the leds into two groups of 12s and make those groups, 3 groups of 4. The 4 leds would be in parallel while the 3 groups would be in series. I felt this would be the best thing to do as I didn't want the current to be too high for safety and handling reasons. Additionally, didn't want to have to work with high voltage. This was important as this would determine the power supply on the regulators. 

PDF: PowerSupplyWhiteLed is what I imagine it to be looking like in terms of circuit
     PowerSupplyAllLEDS is what I imagine with the infrared connected to the white leds 

all images are rough drafts

## 9/28/23 Design document meeting and writing
Objective: Finish the Design Document
We wrote our design doucment here, I had determined the power supply each system, but I wasn't sure how it would go so I wrote that they would all have their own power supply. Then I would use a voltage regulator like the LM7805 for 5V, LM317 for 2V, and LM7809 for 9V. I also read that they have a 2V dropout voltage which was like I added 2Vs so all of the power supply. Since the LEDs, needed 11V, I couldn't find anything small so I decided to put batteries in seires. The voltage regulators would have a configurations of resistors and conductors around them to help them with the output values. I wrote the design document based on these, with the test, verfication and description. 

LM7805: https://www.ti.com/lit/ds/symlink/lm7800.pdf
LM317: https://www.ti.com/lit/ds/symlink/lm317.pdf
LM7809: https://www.alldatasheet.com/datasheet-pdf/pdf/105706/FAIRCHILD/LM7809.html
batteries I had in mind: https://www.amazon.com/s?k=alkaline+batteries&crid=20TMLSMCFKWHA&sprefix=alkaline+%2Caps%2C268&ref=nb_sb_ss_ts-doa-p_3_9

### 10/2/23 PCB work and Design Document presenation prep
Objective: Start looking into the PCB since the review was on friday and first round was the due the next week, Additionally prep for presentation 

Decided what we would work on for the PCB, like what subsystem should people do

### 10/4/23 Design Documentation Review
Decided what to do with the power because having many different voltages should not mean different batteries. If this was a portable device, just not possible. Also LDO should not work as that is too much power for them to dissipate.

### 10/6/23 PCB Review
Objective: We wanted to get our schematic for our microcontroller and the stuff around it checked
We also started working on our main pcb. I also start to make a rechargable battery circuit as this would help increase our complexity. I pushed my schematic for the rechargable battery circuit called Battery_Charging.kicad_sch. I also pushed the microcontroller circuit called pcb_design.kicad_sch to git 

shunt regulator: https://www.ti.com/product/TL431
MOSFET: https://www.infineon.com/cms/en/product/power/mosfet/n-channel/irfz44n/
Informational video for mosfet: https://www.youtube.com/watch?v=rkbjHNEKcRw 

### 10/8/23 Proposal Rewrite and PCB 
Objective: Go over the proposal to see what we missed points on, work on the PCB for the first round

We redid our proposal to fix the issues that we had that caused us to lose points. After that, we worked on our PCB and finished wiring our pcb. We combined the different schematics that we had made separately when we split up the work and then converted it to PCB. I wired the componenets on the PCB for my rechargeable battery circuit. We fixed the power supply to have the power supply be one set of battery that would power the whole board. Additionally, we used buck converter to step down as they are better at stepping down voltage with less heat dissipatted. 

### 10/9/23 First PCB Submit
Objective: submit our first pcb in time for the first round
Then I wired them all of the componenet together on the PCB. We did run into some issues with the not recongnizable footprint when we ran the DRS. This took a long tThen, we asked our TA about the footprint issue and he said to just send it in and if it passes on PCBway then it should be fine. Therefore, we sent our two pcbs after we confirmed that they pass on PCBway.

### 10/18/23 Order parts
Objective: Since the pcb had come we needed the parts to solder onto it and test it

We started ordering our parts over the last couple of days. We created a list on a spreadsheet and found the parts and where to buy them. Then, submitted them on the my.ece to purhcase so it comes hopefully when the pcb comes.

Additionally, we filled out a schedule that the TA requested to 

### 10/20/23 Design Document Rewrite
Objective: Rewrite the Design Document to fix where we lost points on

We worked on it over the night as everyone had different schedule on this day. We fixed all the issues that were identified like the bullet points of the high leve and the R&V table. Additionally, we edited some of the requirements and verifications as we had for the power supply, regulators, UI and microcontroller. We edited out design document also to reflect our power supply and regulator changes that we made when designing the PCB.

### 10/24/23 Workload meeting
Objective: Meet with head TA and professor Fliflet to talk about the project

During this meeting, we talked with the professor and head TA about our project and the design. The head TA talked about how LEDs can be in parallel so we had to put them series. Additionally, he said we could use a LED boost driver to boost the voltage from the power supply to give sufficient voltage across the 12 LEDs. He also said that we should use a LDO instead of buck because the LDO will do the job and be simplier. He told us to get rid of the LED drivers for the white leds and just make it parallel out of one. Plus, the microcontroller and user interface would be parallel from the LDO with an output of 5V. 

### 10/29/23 Order parts for the new design
Objective: Order new parts since the design changed

Order new parts as our PCB changed significantly from the previous PCB. Since we had to change out LED drivers and our buck converters for the LED boost driver and LDO, we had to order new parts to fit that. 

For the LDO we found: https://www.digikey.com/en/products/detail/taiwan-semiconductor-corporation/TQL851CSV50-RLG/16610554

This would give a fixed output 5Vs with a input range up to 50. 

The power supply remains the same because of the duty cycle that the Boost has to produce.

Vout = output voltage, Vin = input voltage, D = duty cycle

Vout = Vin/(1-D)
36 = 12.6/(1-D)

D = 0.65 which reasonable

Any higher Vin would too much for the LDO to step down and any lower Vin would make the duty cycle unreasonable. 

The schematic is in manogna's repository

### 10/30/23 Wire the new PCB
Objective: Wire the new PCB in time for the next round of orders

After we changed the schematic, we had to rewire the new PCB after we converted the new schematic to the PCB. We wired it and then checked the DRS. We still had the footprint issue, but since it passed the pcbway, we knew it would be fine. Additionally, I found some errors in the rechargeable battery circuit where some of componenet were not grounded correctly and some were only grounded. I fixed it and uploaded the new very onto the gitlab called Battery_Charging__1.kicad_sch.

### 11/1/23 Go over the project
Objective: We went over the project, purchase and started testing code

Go over the project and start using the Arduino Uno that has a ATmega328P microcontroller. We just used a online example for output pwm and taking analog inputs.

PWM: https://www.electronicwings.com/avr-atmega/atmega1632-pwm
ADC: https://www.electronicwings.com/avr-atmega/atmega1632-adc

### 11/9/23 Soldering the PCB
Objective: solder the PCB that came in

We started on the rechargeable battery circuit. We were hand soldered all the parts on. The shunt regulator shape was off but we applied extra solder to connect the leds to the right legs. After soldering, we connected the DC power adapter to the circuit and the correct LED did light up. 

### 11/10/23 Using the Oven
Objective: use the oven to connect the parts onto the main PCB

We used the stencil to put solder paste onto the board and then place the parts onto the pcb before we put it into the oven. We were stuck for a while because we couldn't find a tweezer, but eventually we were able to use the oven. We left the ui since they used pins to get soldered

### 11/13/23 Solder the rest of the Main PCB and LED PCB
Objective: solder the rest of the main PCB and LED PCB

Since we couldn't get a hold of the company for the infrared LED, we had ordered a blue led with the same specifications.
We hand soldered the main PCB like the user interface and isp avr programming pins. We also used the stencil to oven the white leds onto the LED PCB. We then hand soldered the Blue LED but it wouldn't turn on.

### 11/15/23 Fix the blue light and the start tesing the code
Objective: We had to fix the blue light solder and then test the code on the arduino

After unsoldering and using a new blue light, the blue LED was able to work. Then, I started testing code. I ran tests with the input pins. First the input digital pins and then the analog pins. To test we created a breadboard with breadboard compenents for switches and potentiometers. The code was able to read the input, but there was some flickering to 0 in between.

I was testing the code for the pwm output by connecting an LED the pwm output of the pin and running test code. 

PWM output: https://www.ee-diary.com/2021/07/atmega328p-fast-pwm-mode-programming.html

This was my new reference for the pwm output. This was able to work when I connected two pwm outputs one to the blue led on the pcb and another to pwm pin to another led on a breadboard. I could control the lights with the input switches and PWM

### 11/17/23 Translate code onto the microcontroller
Objective: translate code into what pins we used for input and ouput on the ATmega328PB-AU

I had to change the pins that we read and output from as they were chosen before already from the pcb wriing. However, when I changed the register names, there was a compilation errors saying it didn't recognize those registers. 

The code was pushed and called Atmega328(old).ino

### 11/27/23 Getting the stuff to work
Objective: get the code to get programmed onto the microcontroller

I fixed the compilation error over the break. We had to download library online, but I tested quite a few and only one was able to work. 
worked: ATmega328PB-Testing/hardware/atmega328pb/avr/libraries/
didn't work: ATmega328PB Xplained Mini
             Minicore
             Megacore
This was unable to work since it kept saying there was a sync error. We thought it could be the programmer, the soldering, but couldn't burn boot loader. 

The code was pushed and called Atmega328(aborted).ino

### 11/28/23 Day before demo
Objective: get the project to work with just dev board

The LDO also wasn't working as intended. It was doing a 3V drop when we gave values greater than 8 so at 12V we would get 9V. 
We gave up on the microcontroller and we decided to use the microcontroller on the Arduino Uno with rest of the board. 
The user interface worked when we tried to it by itself. 

Then, we tested the blue light(infrared light substitute) by power our pcb and then connecting that to the led PCB. IT was able to light up when we passed 12.6 volts through our pcb and ldo.

We also gave the LED driver for the blue light different pwm signals, and the light was able to change brightness accordingly. (image of blue light pushed, called Blue Light.png)

However, after a while, the LDO didn't drop any voltage anymore. When we removed it, we realize it shorted. We tried to put another one on, but nothing changed. 

Additionally, when we were testing the LED Boost Driver, the PWM we gave it was not matching the PWM we read. This mean there was something wrong with the Boost as well. 

We desoldered to see, but it wasn't working.

Also, for the code, to fix the issue of the flickering, I added a 100ms delay whenever there is a change in the values and read the values again. It was like giving a buffer to double check if the switches and potentiometers actually changed values. 

The code was pushed and called Atmega328.ino

### 11/29/23 Demo
Objective: Salvage something for the demo

We tested the user interface and found the switches were burned out. We then used breadboard switches and the potentiometers to do input signals to the dev board and connected the two LEDs. 

We also learned from Head TA during the Demo that the power supply can do over 50Vs.

### 12/1 Mock presentation
Objective: Get feedback for the slides and presentation

We hadn't practiced before this, we had only made the slides. We were trying to see what could be improved on for the slides and the positioning and notice things that needed to be changed. We also wanted to here what the English grader would say for general feedback

### 12/3 white lights and presentation changes
Objective: See if the white light could turn on using the power supply and then change the presenatation based on the feedback

We put in +18 and -18 and the white light was able to turn on. (image of white light pushed, called White Light.png) Blinding when you don't expect it. We changed the slides based on the feedback. 

### 12/5 Final presentation
Objective: Do last practice and then present and then demo the white lights

We did a last practice before the presentation to see what there is to change like words, slides points. 
Then we presented and then demoed the white lights

### 12/6 Final Report
Objective: Write the final report

We wrote the final report and then recorded the extra credit video. 
